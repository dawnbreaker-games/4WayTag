﻿using System;
using System.IO;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace _4WayTag
{
	public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
	{
		public static SaveData saveData = new SaveData();
		public static string MostRecentSaveFileName
		{
			get
			{
				return PlayerPrefs.GetString("Most recent save file name", null);
			}
			set
			{
				PlayerPrefs.SetString("Most recent save file name", value);
			}
		}
		public static string filePath;
		
		void Start ()
		{
			filePath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Auto-Save";
		}

		static void OnAboutToSave ()
		{
			Achievement.instances = FindObjectsOfType<Achievement>();
			List<string> completeAchievementsNames = new List<string>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				if (achievement.complete)
					completeAchievementsNames.Add(achievement.name);
			}
			saveData.completeAchievementsNames = completeAchievementsNames.ToArray();
			saveData.avatarNames = new string[Player.instance.avatars.Length];
			for (int i = 0; i < saveData.avatarNames.Length; i ++)
				saveData.avatarNames[i] = Player.instance.avatars[i].displayName;
		}
		
		public static void Save (string fileName)
		{
			OnAboutToSave ();
			FileStream fileStream = new FileStream(fileName, FileMode.Create);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(fileStream, saveData);
			fileStream.Close();
			MostRecentSaveFileName = fileName;
		}
		
		public void Load (string fileName)
		{
			print(fileName);
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
			fileStream.Close();
			OnLoad (fileName);
		}

		void OnLoad (string fileName)
		{
			MostRecentSaveFileName = fileName;
			OnLoaded ();
		}

		void OnLoaded ()
		{
			Achievement.instances = FindObjectsOfType<Achievement>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				achievement.Init ();
			}
			if (AvatarSelectMenu.Instance == null)
				return;
			AvatarSelectMenu.avatars = AvatarSelectMenu.instance.avatarsParent.GetComponentsInChildren<Avatar>();
			for (int i = 0; i < AvatarSelectMenu.currentAvatarIndices.Length; i ++)
			{
				Avatar avatar = AvatarSelectMenu.instance.avatarsParent.Find(saveData.avatarNames[i]).GetComponent<Avatar>();
				AvatarSelectMenu.currentAvatarIndices[i] = (byte) AvatarSelectMenu.avatars.IndexOf(avatar);
				AvatarSelectMenu.instance.SetCurrentAvatar (avatar);
			}
		}
		
		public void LoadMostRecent ()
		{
			Load (MostRecentSaveFileName);
		}
		
		[Serializable]
		public struct SaveData
		{
			public string[] completeAchievementsNames;
			public string[] avatarNames;
			public Dictionary<string, float> bestLevelTimesDict;
			public string[] triedAvatars;
			public bool showTutorials;

		}
	}
}