#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace _4WayTag
{
	[ExecuteInEditMode]
	public class SetAxisAngleRotation : EditorScript
	{
		public Transform trs;
		public Vector3 axis;
		public float angle;

		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			trs.rotation = Quaternion.AxisAngle(axis, angle);
		}
	}
}
#else
namespace _4WayTag
{
	public class SetAxisAngleRotation : EditorScript
	{
	}
}
#endif