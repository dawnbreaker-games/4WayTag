#if UNITY_EDITOR
using UnityEngine;

namespace _4WayTag
{
	public class MakeCone : EditorScript
	{
        public Cone cone;
        public int basePointCount;
        public Cone.FaceType faceType;
        public float angleToFirstBasePoint;

		public override void Do ()
		{
            cone.MakeMeshRenderer (basePointCount, faceType, angleToFirstBasePoint);
		}
	}
}
#else
namespace _4WayTag
{
	public class MakeCone : EditorScript
	{
	}
}
#endif