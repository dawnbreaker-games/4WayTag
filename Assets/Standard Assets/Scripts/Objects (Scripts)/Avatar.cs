using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace _4WayTag
{
	public class Avatar : Entity
	{
		public string displayName;
		public Transform itemsParent;
		public Item[] items = new Item[0];
		public UseableItem[] useableItems = new UseableItem[0];
		public Transform cooldownIndicatorsParent;
		public Animator animator;
		public bool unlocked;
		public Achievement unlockOnCompleteAchievement;
		public GameObject lockedIndicatorGo;
		public GameObject untriedIndicatorGo;
		public Transform destinationTrs;
		public Action<Collider2D> onCollide;
		public GameObject enabledIndicator;
		public static Avatar instance;
		public static Avatar Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Avatar>(true);
				return instance;
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			if (unlockOnCompleteAchievement == null || unlockOnCompleteAchievement.complete || unlockOnCompleteAchievement.ShouldBeComplete())
			{
				unlocked = true;
				lockedIndicatorGo.SetActive(false);
				untriedIndicatorGo.SetActive(!SaveAndLoadManager.saveData.triedAvatars.Contains(name));
			}
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			instance = this;
			destinationTrs.SetParent(null);
			items = itemsParent.GetComponentsInChildren<Item>();
			useableItems = itemsParent.GetComponentsInChildren<UseableItem>();
			for (int i = 0; i < items.Length; i ++)
			{
				Item item = items[i];
				if (!item.gameObject.activeSelf)
					continue;
				UseableItem useableItem = item as UseableItem;
				if (useableItem != null)
					useableItem.useAction.performed += useableItem.TryToUse;
				item.OnGain ();
			}
			enabledIndicator.SetActive(true);
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			base.DoUpdate ();
			HandleSetDestination ();
		}

		public override void HandleMoving ()
		{
			if (InputManager.MovementInput.sqrMagnitude >= InputManager.instance.minPressMagnitude * InputManager.instance.minPressMagnitude)
			{
				destinationTrs.gameObject.SetActive(false);
				Move (InputManager.MovementInput * moveSpeed);
			}
			else if (destinationTrs.gameObject.activeSelf)
				Move ((destinationTrs.position - trs.position).normalized * moveSpeed);
			else
				Move (Vector2.zero);
		}

		public override void TakeDamage (float amount)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, maxHp);
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public override void Death ()
		{
			GameMode.instance.End ();
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			enabledIndicator.SetActive(false);
		}

		void HandleSetDestination ()
		{
			if (InputManager.LeftClickInput)
			{
				destinationTrs.position = CameraScript.instance.camera.ScreenToWorldPoint(Mouse.current.position.ReadValue()).SetZ(0);
				destinationTrs.gameObject.SetActive(true);
			}
		}

		void Move (Vector2 move)
		{
			move = Vector2.ClampMagnitude(move, moveSpeed);
			if (destinationTrs.gameObject.activeSelf)
			{
				float moveAmount = move.magnitude * Time.deltaTime;
				Vector2 toDestination = destinationTrs.position - trs.position;
				if (toDestination.sqrMagnitude > moveAmount * moveAmount)
					rigid.velocity = move;
				else
				{
					rigid.velocity = Vector2.zero;
					trs.position = destinationTrs.position;
				}
			}
			else
				rigid.velocity = move;
			if (move != Vector2.zero)
				trs.up = move;
		}

		void OnCollisionEnter2D (Collision2D coll)
		{
			if (onCollide != null)
				onCollide (coll.collider);
		}
	}
}