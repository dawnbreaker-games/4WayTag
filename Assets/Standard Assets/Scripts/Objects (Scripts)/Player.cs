using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace _4WayTag
{
	public class Player : SingletonUpdateWhileEnabled<Player>
	{
		public uint id;
		public uint currentAvatarIndex;
		public Avatar[] avatars = new Avatar[0];

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			base.DoUpdate ();
			if (Keyboard.current.spaceKey.wasPressedThisFrame)
			{
				currentAvatarIndex ++;
				if (currentAvatarIndex >= avatars.Length)
					currentAvatarIndex = 0;
				avatars[currentAvatarIndex].enabled = true;
				Avatar.instance.enabled = false;
			}
		}
	}
}