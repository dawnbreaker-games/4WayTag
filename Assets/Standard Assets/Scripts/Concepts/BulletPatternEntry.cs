using System;
using UnityEngine;

namespace _4WayTag
{
	[Serializable]
	public class BulletPatternEntry
	{
		public string name;
		public BulletPattern bulletPattern;
		public Bullet bulletPrefab;
		public Transform spawner;
		
		public virtual Bullet[] Shoot ()
		{
			return bulletPattern.Shoot(spawner, bulletPrefab);
		}
	}
}