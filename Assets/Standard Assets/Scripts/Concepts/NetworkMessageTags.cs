public class NetworkMessageTags
{
	public const byte BEGIN_GAME = 0;
	public const byte SPAWN_PLAYER = 1;
	public const byte AVATAR_MOVED = 2;
	public const byte PLAYER_LEFT = 3;
}