using Extensions;

namespace _4WayTag
{
	public class Heart : Item
	{
		public override void OnGain ()
		{
			owner.TakeDamage (-1);
			owner.items = owner.items.Remove(this);
			Destroy(gameObject);
		}
	}
}