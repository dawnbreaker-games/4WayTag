using Extensions;
using UnityEngine;

namespace _4WayTag
{
	public class MagicMirror : UseableItem, IUpdatable
	{
		public Transform mirrorPositionIndicatorTrs;

		void OnEnable ()
		{
			mirrorPositionIndicatorTrs.SetParent(null);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		void OnDisable ()
		{
			if (mirrorPositionIndicatorTrs != null)
				mirrorPositionIndicatorTrs.gameObject.SetActive(false);
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void DoUpdate ()
		{
			mirrorPositionIndicatorTrs.position = -owner.trs.position;
			mirrorPositionIndicatorTrs.up = -owner.trs.up;
		}

		public override void Use ()
		{
            base.Use ();
			owner.trs.position *= -1;
		}
	}
}