using Extensions;
using UnityEngine;

namespace _4WayTag
{
	public class AddToAnimatorParameterItem : Item
	{
		public string parameterName;
		public float amount;

		public override void OnGain ()
		{
			Animator animator = owner.animator;
			animator.SetFloat(parameterName, animator.GetFloat(parameterName) + amount);
		}
	}
}