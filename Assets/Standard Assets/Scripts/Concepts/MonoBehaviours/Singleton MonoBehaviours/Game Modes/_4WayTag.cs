using UnityEngine;

namespace _4WayTag
{
	public class _4WayTag : GameMode
	{
		public new static _4WayTag instance;
		public new static _4WayTag Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<_4WayTag>();
				return instance;
			}
		}
		public Avatar[] avatars = new Avatar[4];
		public _LineRenderer[] _lineRenderers = new _LineRenderer[4];
		public bool avatar1And3AreLocal;

		public override void Begin ()
		{
			base.Begin ();
			Avatar avatar1 = avatars[0];
			Avatar avatar2 = avatars[1];
			Avatar avatar3 = avatars[2];
			Avatar avatar4 = avatars[3];
			avatar1.onCollide = (Collider2D collider) => { Avatar avatar = collider.GetComponentInParent<Avatar>(); if (avatar == avatar4) avatar1.Death (); };
			avatar2.onCollide = (Collider2D collider) => { Avatar avatar = collider.GetComponentInParent<Avatar>(); if (avatar == avatar1) avatar2.Death (); };
			avatar3.onCollide = (Collider2D collider) => { Avatar avatar = collider.GetComponentInParent<Avatar>(); if (avatar == avatar2) avatar3.Death (); };
			avatar4.onCollide = (Collider2D collider) => { Avatar avatar = collider.GetComponentInParent<Avatar>(); if (avatar == avatar3) avatar4.Death (); };
			if (avatar1And3AreLocal)
			{
				avatar1.enabled = true;
				avatar3.enabled = true;
			}
			else
			{
				avatar2.enabled = true;
				avatar4.enabled = true;
			}
			_lineRenderers[0].points = new Transform[] { avatar1.trs, avatar2.trs };
			_lineRenderers[1].points = new Transform[] { avatar2.trs, avatar3.trs };
			_lineRenderers[2].points = new Transform[] { avatar3.trs, avatar4.trs };
			_lineRenderers[3].points = new Transform[] { avatar4.trs, avatar1.trs };
		}
	}
}