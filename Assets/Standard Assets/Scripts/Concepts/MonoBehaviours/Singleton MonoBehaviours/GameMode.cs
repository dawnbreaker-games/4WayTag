namespace _4WayTag
{
	public class GameMode : SingletonMonoBehaviour<GameMode>
	{
		public string displayName;
		public Player playerPrefab;

		public virtual void Begin ()
		{
		}

		public virtual void End ()
		{
			_SceneManager.instance.RestartScene ();
		}
	}
}