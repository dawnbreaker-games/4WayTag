using UnityEngine;

namespace _4WayTag
{
	public class SettingsMenu : Menu
	{
		public new static SettingsMenu instance;
		public new static SettingsMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<SettingsMenu>(true);
				return instance;
			}
		}
	}
}