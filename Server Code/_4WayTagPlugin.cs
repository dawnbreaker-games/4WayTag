using System;
using DarkRift;
using DarkRift.Server;
using System.Collections;
using System.Collections.Generic;

namespace _4WayTag
{
	public class _4WayTagPlugin : Plugin
	{
		public override Version Version => new Version(1, 0, 0);
		public override bool ThreadSafe => true;
		public static _4WayTagPlugin instance;
		public static Vector2[] spawnPoints = new Vector2[] { Vector2.One * 7.5f, -Vector2.One * 7.5f, new Vector2(-7.5f, 7.5f), new Vector2(7.5f, -7.5f) };
		static Random random = new Random();
		static Dictionary<uint, Player> playersDict = new Dictionary<uint, Player>();

		public _4WayTagPlugin (PluginLoadData pluginLoadData) : base (pluginLoadData)
		{
			instance = this;
			ClientManager.ClientConnected += OnClientConnected;
			ClientManager.ClientDisconnected += OnClientDisconnected;
		}

		void OnClientConnected (object sender, ClientConnectedEventArgs eventArgs)
		{
			foreach (Player previousPlayer in playersDict.Values)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(previousPlayer.id);
					writer.Write(previousPlayer.avatars[0].index);
					writer.Write(previousPlayer.avatars[1].index);
					writer.Write(previousPlayer.avatars[0].position.x);
					writer.Write(previousPlayer.avatars[0].position.y);
					writer.Write(previousPlayer.avatars[1].position.x);
					writer.Write(previousPlayer.avatars[1].position.y);
					using (Message message = Message.Create(NetworkMessageTags.SPAWN_PLAYER, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
			}
			eventArgs.Client.MessageReceived += OnBeginGameMessageReceived;
		}

		void OnClientDisconnected (object sender, ClientDisconnectedEventArgs eventArgs)
		{
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				writer.Write(eventArgs.Client.ID);
				using (Message message = Message.Create(NetworkMessageTags.PLAYER_LEFT, writer))
				{
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						if (client != eventArgs.Client)
							client.SendMessage(message, SendMode.Reliable);
					}
				}
			}
			playersDict.Remove(eventArgs.Client.ID);
			eventArgs.Client.MessageReceived -= OnMessageReceived;
			eventArgs.Client.MessageReceived -= OnBeginGameMessageReceived;
		}

		void OnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				if (message.Tag == NetworkMessageTags.AVATAR_MOVED)
					OnAvatarMoved (eventArgs);
			}
		}

		void OnBeginGameMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			OnBeginGame (eventArgs);
			eventArgs.Client.MessageReceived -= OnBeginGameMessageReceived;
			eventArgs.Client.MessageReceived += OnMessageReceived;
		}

		void OnBeginGame (MessageReceivedEventArgs eventArgs)
		{
			DarkRiftReader reader = eventArgs.GetMessage().GetReader();
			byte avatar1Index = reader.ReadByte();
			byte avatar2Index = reader.ReadByte();
			Vector2 avatar1SpawnPoint;
			Vector2 avatar2SpawnPoint;
			if (playersDict.Count % 2 == 0)
			{
				avatar1SpawnPoint = spawnPoints[0];
				avatar2SpawnPoint = spawnPoints[1];
			}
			else
			{
				avatar1SpawnPoint = spawnPoints[2];
				avatar2SpawnPoint = spawnPoints[3];
			}
			Avatar avatar1 = new Avatar(avatar1Index, avatar1SpawnPoint);
			Avatar avatar2 = new Avatar(avatar2Index, avatar2SpawnPoint);
			Player player = new Player(eventArgs.Client.ID, new Avatar[] { avatar1, avatar2 });
			DarkRiftWriter writer = DarkRiftWriter.Create();
			writer.Write(player.id);
			writer.Write(avatar1Index);
			writer.Write(avatar2Index);
			writer.Write(player.avatars[0].position.x);
			writer.Write(player.avatars[0].position.y);
			writer.Write(player.avatars[1].position.x);
			writer.Write(player.avatars[1].position.y);
			Message message = Message.Create(NetworkMessageTags.SPAWN_PLAYER, writer);
			IClient[] clients = ClientManager.GetAllClients();
			for (int i = 0; i < clients.Length; i ++)
			{
				IClient client = clients[i];
				client.SendMessage(message, SendMode.Reliable);
			}
			playersDict.Add(player.id, player);
		}

		void OnAvatarMoved (MessageReceivedEventArgs eventArgs)
		{
			Message message = eventArgs.GetMessage();
			DarkRiftReader reader = message.GetReader();
			Player player = playersDict[reader.ReadUInt32()];
			bool isFirstAvatar = reader.ReadBoolean();
			if (isFirstAvatar)
				player.avatars[0].position = new Vector2(reader.ReadSingle(), reader.ReadSingle());
			else
				player.avatars[1].position = new Vector2(reader.ReadSingle(), reader.ReadSingle());
			IClient[] clients = ClientManager.GetAllClients();
			for (int i = 0; i < clients.Length; i ++)
			{
				IClient client = clients[i];
				if (client != eventArgs.Client)
					client.SendMessage(message, eventArgs.SendMode);
			}
		}
	}
}