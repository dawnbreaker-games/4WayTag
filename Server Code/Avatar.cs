public class Avatar
{
    public byte index;
    public Vector2 position;

    public Avatar (byte index, Vector2 position)
    {
        this.index = index;
        this.position = position;
    }
}